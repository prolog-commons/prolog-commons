# ---------------------------------------------------------------------------
# Fake associative arrays in bash (<4)

mset() {
  eval `echo m__${1}__${2}`='"'"$3"'"'
}

mget() {
  eval echo '${m__'${1}__${2}'}'
}

