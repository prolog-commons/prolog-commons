# List of available systems

systems="swi yap XSB gprolog ciao eclipse"

# ---------------------------------------------------------------------------

mset swi name "SWI Prolog (GIT)"
mset swi method git
mset swi url "git://prolog.cs.vu.nl/home/pl/git/pl-devel.git"
function swi_compile() {
    if [ "`uname`" = Darwin ]; then
	# Avoid problems in Mac OS X with macports and readline
	export LIBRARY_PATH=/usr/lib:/opt/local/lib
	export CPATH=/usr/include:/opt/local/include
	export PKG_CONFIG_PATH=/usr/X11R6/lib/pkgconfig:/opt/local/lib/pkgconfig
    fi
    ./prepare --yes
    ./configure --prefix=${self}/systems
    make
    make install
}
function swi_toplevel() {
    ${self}/systems/bin/swipl
}

mset yap name "YAP Prolog (GIT)"
mset yap method git
mset yap url "git://yap.dcc.fc.up.pt/yap-6"
function yap_compile() {
    ./configure --prefix=${self}/systems
    make
    make install
}
function yap_toplevel {
    ${self}/systems/bin/yap
}

# ---------------------------------------------------------------------------

if [ x"${COMMONS_XSB_VERSION}" == x"" ]; then
    XSB_version="3.3.8" # Defines default XSB version
else
    XSB_version="${COMMONS_XSB_VERSION}"
fi
mset XSB name "XSB Prolog (CVS)"
mset XSB method cvs
mset XSB url "anonymous@xsb.cvs.sourceforge.net:/cvsroot/xsb"
mset XSB cvs_module "xsbdevel"
function XSB_compile() {
    cd build
    ./configure --prefix=${self}/systems
    ./makexsb
    ./makexsb install
}
function XSB_toplevel() {
    ${self}/systems/xsb-${XSB_version}/bin/xsb
}

# ---------------------------------------------------------------------------

mset gprolog name "GNU Prolog (GIT)"
mset gprolog method git
mset gprolog url "git://git.code.sf.net/p/gprolog/code"
function gprolog_compile() {
    cd src
    autoconf
    ./configure --with-install-dir=in-place --with-links-dir=${self}/systems/bin
    make
    make install-system
    make install-links
}
function gprolog_toplevel() {
    ${self}/systems/bin/gprolog
}

# ---------------------------------------------------------------------------

# if [ x"${COMMONS_CIAO_VERSION}" == x"" ]; then
#     ciao_version=${default_ciao_version}
# else
#     ciao_version="${COMMONS_CIAO_VERSION}"
# fi
# mset ciao name "Ciao Prolog (${ciao_version})"
# if [ x"${ciao_version}" == x"SVN" ]; then
#     mset ciao method svn
#     mset ciao url "svn+ssh://clip.dia.fi.upm.es/home/clip/SvnReps/Systems/CiaoDE/trunk"
# else
#     mset ciao method tgz
#     mset ciao url "http://www.ciaohome.org/packages/trunk/${ciao_version}/CiaoDE-1.15.0-${ciao_version}.tar.gz"
#     mset ciao tgz_root "CiaoDE-1.15.0-${ciao_version}"
# fi

case x"${COMMONS_CIAO_VERSION}" in 
    x"")
	mset ciao name "Ciao Prolog (`date "+%Y-%m-%d"`)"
	mset ciao method tgz
	mset ciao url "http://www.ciaohome.org/packages/trunk/CiaoDE-latest.tar.gz"
	# mset ciao tgz_root "`'ls' -t | grep --max-count=1 CiaoDE-1.15.0`"
	mset ciao tgz_root "CiaoDE-1.15*"
	;;
    x"SVN")
	mset ciao name "Ciao Prolog (SVN: `date "+%Y-%m-%d-%H:%M"`)"
	mset ciao method svn
	mset ciao url "svn+ssh://clip.dia.fi.upm.es/home/clip/SvnReps/Systems/CiaoDE/trunk"
	;;
    *)
	mset ciao name "Ciao Prolog (1.15.0-${COMMONS_CIAO_VERSION})"
	mset ciao method tgz
	mset ciao url "http://www.ciaohome.org/packages/trunk/${COMMONS_CIAO_VERSION}/CiaoDE-1.15.0-${COMMONS_CIAO_VERSION}.tar.gz"
	mset ciao tgz_root "CiaoDE-1.15.0-${COMMONS_CIAO_VERSION}"
	;;
esac
function ciao_compile() {
    ./ciaosetup clean_config
#    ./ciaosetup configure --destdir=${self}/systems --instype=local --registration_type=user
    ./ciaosetup configure --instype=local --registration_type=user
    ./ciaosetup build
    ./ciaosetup docs
    ./ciaosetup install
}
function ciao_toplevel() {
    ${self}/systems/ciao/build/bin/ciao
}

# ---------------------------------------------------------------------------

mset eclipse name "eclipse Prolog (CVS)"
mset eclipse method cvs
mset eclipse url "anonymous@eclipse-clp.cvs.sourceforge.net:/cvsroot/eclipse-clp"
mset eclipse cvs_module "Eclipse"
function eclipse_compile() {
    detect_eclipse_arch
    ./configure --prefix=${self}/systems
    make -f Makefile.$ARCH
    make -f Makefile.$ARCH install
}
function eclipse_toplevel() {
    detect_eclipse_arch
    ${self}/systems/bin/$ARCH/eclipse
}

function detect_eclipse_arch() {
    ARCH=`${self}/systems/${system}/ARCH`
    export ARCH
}
