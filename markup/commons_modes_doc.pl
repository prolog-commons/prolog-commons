:- use_package(assertions).
:- doc(filetype,package).
:- doc(nodoc,assertions).

:- doc(title,"Modes used in the Commons Library").
:- doc(author,"Manuel Hermenegildo").
:- doc(author,"Jose F. Morales").
:- doc(nodoc,assertions).

:- doc(module,"This file contains the modes proposed for the commons
   library. They are similar to the ones used in the ISO standard and
   in recent proposals.").

:- doc(bug,"Need to add ref to Jan et al's paper, etc.").

% :- include(library(dialect(commons_modes))).

%% Only local meaning
:- doc(hide,call/2).
:- prop call(X,A) # "Argument @var{A} has property @var{X} (e.g., int(A)).".

call(_,_).

:- use_module(engine(hiord_rt)).

:- op(200, fy, [(?),(@),(:)]).

%% Basic ISO-modes
:- modedef '+'(A) : nonvar(A) # "The classic way to mark an input argument.".
:- modedef '-'(A) : var(A) # "The classic way to mark an output argument.".
%% The standard says that this should be:
% :- modedef '-'(A) : var(A) => nonvar(A).
%% but then it says that the only error possible is for not 
%% meeting the : var... what to do?
:- modedef '?'(_).
:- modedef '@'(A) + not_further_inst(A).
%% Only in older versions of standard? It is obsolete now.
%% :- modedef '*'(_).
%% Meta arg. Needs to be defined further?
:- modedef ':'(A) : nonvar(A) # "Is a meta-argument (implies +)".


%% Parametric versions of above
:- push_prolog_flag(read_hiord,on).
:- modedef +(A,X) :  X(A).
:- modedef -(A,X) :  var(A) => X(A).
%% Version in standard supports this simple interpretation:
% :- modedef ?(A,X) :: X(A).
%% but all builtins conform to:
:- modedef ?(A,X) :: X(A) => X(A).
%% ..what to do??
:- modedef @(A,X) :  X(A) => X(A) + not_further_inst(A).
%% Only in older versions of standard? It is obsolete now.
%% :- modedef *(A,X) :: X(A).
%% Meta arg. Needs to be defined further?
:- modedef ':'(A,X) : X(A) # "Is a meta-argument (implies +)".
:- pop_prolog_flag(read_hiord).

