#!/bin/bash
#
# A simple script to fetch the code of different Prolog systems,
# compile, and launch them.
#
#    (use it to automate tests in different machines)
#
# Please, look at common-prolog__modules/systems.sh for adding or
# updating Prolog systems.
#
# Authors:
#   Jose F. Morales <jfmcjf@gmail.com>
#   Manuel Hermenegildo <herme@fi.upm.es>
#
# 
# ---------------------------------------------------------------------------
# Obtain the directory where this script is located
old_dir=`pwd`; cd `dirname $0`; self=`pwd`; cd ${old_dir}; old_dir=

# ---------------------------------------------------------------------------

moddir="${self}/common-prolog__modules"

source ${moddir}/common.sh
source ${moddir}/systems.sh

# ---------------------------------------------------------------------------
# Getting/updating the sources

fetch_system() { # fetch_system(system)
    local system=$1
    local method
    if [ -d ${system} ]; then
	echo ":: Updating `mget ${system} name`."
	pushd ${system} > /dev/null
	case `mget ${system} method` in
	    git) git pull ;;
	    svn) svn update ;;
	    cvs) cvs update ;;
	    tgz) echo "ERROR: cannot update from tgz. Please remove systems/${system} first." ;;
	esac
	popd > /dev/null
	echo ":: Done updating `mget ${system} name`."
	return
    else
	echo ":: Getting `mget ${system} name`."
#	mkdir -p ${system}
#	pushd ${system} > /dev/null
	method="`mget ${system} method`"
	case "${method}" in
	    git)
		git clone `mget ${system} url` ${system}
		;;
	    svn)
		svn checkout `mget ${system} url` ${system}
		;;
	    cvs)
		cvs -z3 -d:pserver:`mget ${system} url` co -P `mget ${system} cvs_module`
		;;
	    tgz)
		curl -q `mget ${system} url` | tar -zxf -
		mv `mget ${system} tgz_root` ${system}
		;;
	    *)
		echo "ERROR: Unknown fetch method ${method}."
		exit 0
		;;
	esac
#	popd > /dev/null
	echo ":: Done getting `mget ${system} name`."
    fi
}

fetch() {
    if [ x"$1" == x"${all_systems_id}" ]; then
	all="${systems}"
    else
	all="$1"
    fi
    #
    mkdir -p ${self}/systems
    cd ${self}/systems
    for system in ${all}; do
	fetch_system ${system}
    done
}

compile() {
    if [ x"$1" == x"${all_systems_id}" ]; then
	all="${systems}"
    else
	all="$1"
    fi
    #
    for system in ${all}; do
	echo ":: Compiling `mget ${system} name`."
	pushd ${self}/systems/${system} > /dev/null
	eval "${system}_compile"
	popd > /dev/null
	echo ":: Done compiling `mget ${system} name`."
    done
}

all_systems_id="all"
help() {
    cat <<EOF
Usage: `basename ${0}` [fetch SYSLIST|compile SYSLIST|toplevel SYSTEM]
Where
  SYSLIST is a list of SYSTEMs (or '${all_systems_id}' for all)
  SYSTEM is one of: ${systems}

Caveats/TODO:

  - Default revision numbers for XSB is hardwired, but
    can be defined with environment variables COMMONS_XSB_VERSION.
  - Ciao can be downloaded from tar.gz or SVN. Default is: fetch latest
    ROTD by tar.gz. Otherwise set the COMMONS_CIAO_VERSION enviroment
    variable to the desired version or to "SVN".
EOF
}

toplevel() {
    local system=$1
    eval "${system}_toplevel"
}

case $1 in
    fetch) shift; fetch $* ;;
    compile) shift; compile $* ;;
    toplevel) shift; toplevel $*;;
    *) help ;;
esac

