:- module(_, _, [ciaopaths, assertions, regtypes, fsyntax]).

:- include(lpdoclib('SETTINGS_schema')).
% ****************************************************************************
% This is an LPdoc configuration file. See SETTINGS_schema for documentation *
% ****************************************************************************

:- use_module(library(system)).
:- use_module(library(terms), [atom_concat/2]).

:- doc(title, "Prolog Commons Manual/Web Site Settings").

:- doc(module, "This file contains the definitions and configuration
   settings for the Prolog Commons manual and web site.").

:- doc(author, "Manuel Hermenegildo").
:- doc(author, "Jose Morales").

:- doc(filetype, user).

% ----------------------------------------------------------------------------

% Value provided from the command line ("-d commonsroot=STRING")
:- use_module(library(make(make_rt)), [get_value/2]).
:- use_module(library(lpdist(makedir_aux)), [fsR/2]).

commonsroot := ~get_value(commonsroot).
ciaolibroot := ~get_value(ciaolibroot).

% TODO: This is defined automatically by lpdoc, but not accessible here. Fix
lpdoclib := ~get_value(lpdoclib).

datamode(perm(rw, rw, r)).
execmode(perm(rwx, rwx, rx)).

% REVIEW
filepath := 
	( ~atom_concat(~commonsroot,'library')
	| ~atom_concat(~commonsroot,'library/heaps')
	| ~atom_concat(~commonsroot,'library/random') 
	| ~atom_concat(~commonsroot,'undecided')
	| ~atom_concat(~commonsroot,'undecided/heaps')
	| ~atom_concat(~commonsroot,'undecided/queues')
	| ~atom_concat(~commonsroot,'markup')
	| ~atom_concat(~commonsroot,'doc')
	).

systempath :=
	( ~atom_concat(~ciaolibroot,'lib/assertions')
	| ~atom_concat(~ciaolibroot,'lib/metaprops')
	| ~atom_concat(~ciaolibroot,'lib/regtypes')
	| ~atom_concat(~ciaolibroot,'lib/engine')
	| ~atom_concat(~ciaolibroot,'lib')
	| ~atom_concat(~ciaolibroot,'library')
	| ~atom_concat(~ciaolibroot,'contrib')
	| ~atom_concat(~ciaolibroot,'contrib/doccomments')
	).

pathsfile(_) :- fail.  % kludge to avoid compilation error

output_name := 'PrologCommons'.

doc_structure := 
    'PrologCommons'-
        ['part_library'-
	   [
	    'assoc'
           ,'atoms'
	   ,'c_aggregate'
 	   ,'c_arith'
           ,'c_commas'
           ,'codesio'
           ,'coroutining'
           ,'gensym'
           ,'binomialheap_new'  % Changed module name to binomialheap_new
           ,'c_lists'
           ,'multisets'
           ,'c_numlists'
           ,'occurs'
           ,'c_ordsets'
%%           ,'c_pairs' % :- if(\+(current_prolog_flag(dialect,xsb))).
%%           ,'random.P'
           ,'readutils'
           ,'sets'
           ,'statistics'
%%            ,'c_terms' % :- if(current_prolog_flag(dialect,xsb)).
	   ]
        , 'part_undecided'-
	   [
            'dcg_basics'
           ,'hashtable'
           , 'binomialheap'
           ,'heaps'
           ,'integertable' % library('dialect/hprolog')
           ,'pairlist'
%%            ,'pure_input' % :- use_module(library(option)).
           ,'queues.dec10.pl' % Changed module name to 'queues.dec10'
           ,'queues'
%%          ,'stdscan' % usermod does not exist
	   ]
        , 'part_markup_testing'-
 	   [
	     'markup_language'
	   , 'commons_modes_doc'
	   , 'c_aggregate_test' % Just testing
	   ]   
	].

% doc_structure := 'PrologCommons'.

commonopts   := v | modes | no_patches | no_engmods | no_sysmods | no_packages | no_math | no_lpdocack.
doc_mainopts := ~commonopts.
doc_compopts := ~commonopts.

docformat := html|texi|ps|pdf|manl|info.

index := concept.
index := lib.
% index := apl.
index := pred.
index := prop.
index := regtype.
% index := decl.
% index := op.
index := modedef.
% index := file.
index := global.
index := author.
% index := all.

bibfile := '/home/clip/bibtex/clip/clip'.
bibfile := '/home/clip/bibtex/clip/others'.

startpage := 1.

papertype := afourpaper.

libtexinfo := 'yes'.

infodir_headfile := ~atom_concat([~lpdoclib ,'Head_clip.info']).
infodir_tailfile := ~atom_concat([~lpdoclib, 'Tail_clip.info']).

htmldir(_) :- fail.
docdir(_) :- fail.
infodir(_) :- fail.
mandir(_) :- fail.

% ----------------------------------------------------------------------------
% End of SETTINGS
% ----------------------------------------------------------------------------
