:- use_package(assertions).

:- doc(title,"The Prolog Commons").

:- doc(subtitle,
	"@em{A Common, Public Domain Library for Prolog Systems}").
:- doc(subtitle,"").
:- doc(subtitle,"").
:- doc(subtitle,"REFERENCE MANUAL").
:- doc(subtitle,"").
:- doc(subtitle,"@em{Generated/Printed on:} @today{}").

:- doc(author, 
	"The Prolog Commons Group @hfill 
	 @href{http://www.prolog-commons.org/}").

:- doc(summary,"The Prolog-Commons Working Group has formed in
   order to develop a common, public-domain set of libraries for
   systems that support the Prolog language. These pages document the
   common libraries developed so far.").

:- doc(module," 

Have you ever tried to port a complex Prolog program from one Prolog
system to another?  The ISO Prolog standard makes this easy, right?

As anyone knows who has ever attempted such a port, the answer is
usually ""no"".  The core ISO Prolog standard, while a significant
achievement, is limited in its scope.  It does not address libraries
interfaces, constraint systems, or number of other features that real
applications need.  The lack of a broad-based standard is not the only
impediment to portability.  It actually may be impossible to move
between Prolog systems if your application requires features such as
tabling, native code generation, dynamic indexing, compiler analysis
or an interactive development environment.  To some extent, this
diversity is a result of the health of logic programming, as there are
numerous fine Prolog systems, each of which has a rapidly evolving
code base and specializes in diverse and important research
directions.  However the limited portablity of Prolog code, along
with the lack of a single Prolog system to which all researchers
contribute, can also be a barrier for potential users of logic
programming.

The Prolog-Commons Working Group has formed in order to start to
address some of these issues. In particular, it is currently working
on a common, public-domoin set of libraries. This manual documents the
common libraries developed so far.

The first meeting of the group,
organized by Bart Demoen, occurred in Leuven, Belgium on February
12-14, and was attended by Manuel Hermenegildo and Jos@'{e} Morales of
Ciao Prolog, Jan Wielemaker of SWI Prolog, V@'{i}tor Santos Costas of
YAP Prolog, Daniel Diaz and Salvador Abreu of GNU Prolog , and
Terrance Swift of XSB Prolog.  Each of the Prolog mentioned has unique
features that the others do not, and as a result all have their own
dedicated user communities.

The first day of the meeting was somewhat informal.  An important
psychological milestone was passed when all implementors managed to
install one another's Prologs on their laptops.  A common repository
for working code was also created, and discussion began on what
exactly to do with the repository.  The group quickly agreed on
general standards for using modules in libraries, for adding
annotations about types, and for unit testing.  At the same time,
consensus for documenting code in a ""literate programming"" style
proved more difficult, although the group achieved a provisional
agreement on this as well.  The second and third days built on the
progress of the first, when the attendees began to actually add code
and APIs to the repository, which by the end of the meeting contained
nearly 4000 lines of code.

Creation of a common set of sophisticated libraries and packages for
Prolog is a giant task, of which the Leuven meeting was (hopefully)
the first step.  However, in addition to the concrete progress made on
the repository, the attendees agreed that there was a great deal of
intangible benefit in understanding both one another's systems and how
to write more portable library code.

The next meeting of the working group is planned to take place near
the time of ICLP 2009 in Pasadena California.
").

:- doc(copyright,"Copyright @copyright{} The Prolog Commons Group

This document may be freely read, stored, reproduced, disseminated, translated 
or quoted by any means and on any medium provided the following conditions 
are met:

@begin{enumerate}

@item Every reader or user of this document acknowledges that is aware that no 
guarantee is given regarding its contents, on any account, and specifically 
concerning veracity, accuracy and fitness for any purpose.

@item No modification is made other than cosmetic, change of
representation format, translation, correction of obvious syntactic
errors, or as permitted by the clauses below.

@item Comments and other additions may be inserted, provided they
clearly appear as such; translations or fragments must clearly refer
to an original complete version, preferably one that is easily
accessed whenever possible.

@item Translations, comments and other additions or modifications must
be dated and their author(s) must be identifiable (possibly via an
alias).

@item This licence is preserved and applies to the whole document with
modifications and additions (except for brief quotes), independently
of the representation format.

@item Any reference to the ""official version"", ""original version"" or
""how to obtain original versions"" of the document is preserved
verbatim. Any copyright notice in the document is preserved
verbatim. Also, the title and author(s) of the original document
should be clearly mentioned as such.

@item In the case of translations, verbatim sentences mentioned in (6.) are 
preserved in the language of the original document accompanied by verbatim 
translations to the language of the traslated document. All translations 
state clearly that the author is not responsible for the translated work. 
This license is included, at least in the language in which it is referenced 
in the original version.


@item Whatever the mode of storage, reproduction or dissemination, anyone able 
to access a digitized version of this document must be able to make a 
digitized copy in a format directly usable, and if possible editable, 
according to accepted, and publicly documented, public standards.

@item Redistributing this document to a third party requires simultaneous 
redistribution of this licence, without modification, and in particular 
without any further condition or restriction, expressed or implied, related 
or not to this redistribution. In particular, in case of inclusion in a 
database or collection, the owner or the manager of the database or the 
collection renounces any right related to this inclusion and concerning the 
possible uses of the document after extraction from the database or the 
collection, whether alone or in relation with other documents.

@end{enumerate}

Any incompatibility of the above clauses with legal, contractual or 
judiciary decisions or constraints implies a corresponding limitation 
of reading, usage, or redistribution rights for this document, 
verbatim or modified.
").

main.
