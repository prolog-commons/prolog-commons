
:- use_package(assertions).

:- doc(filetype, part).

:- doc(title,"PART II - Undecided").

:- doc(author, "The Prolog Commons Group").

:- doc(module,"This part contains modules that are proposed for
   adoption but over which there is no general agreement yet.").

main.


