
:- use_package(assertions).

:- doc(filetype, part).

:- doc(title,"PART III - Markup Language and Test Files").

:- doc(author, "The Prolog Commons Group").

:- doc(module,"This part contains example modules to test (and provide
   examples for) the markup language used as well as a description of
   the markup language itself.").

main.


