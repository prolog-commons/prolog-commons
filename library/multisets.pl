:- module(c_multisets,
	  [ powerset/2,
	    cross_product/2
	  ]).
:- include(library('dialect/commons')).

