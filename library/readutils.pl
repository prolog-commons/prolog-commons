:- module(c_readutils,
	  [ read_line_to_codes/2,
	    read_line_to_codes/3,	% difflist version
	    read_stream_to_codes/2,
	    read_to_bytes/4,		% +Stream, +Count, -Read, -Codes
	    read_to_bytes/5,		% +Stream, +Count, -Read, -Codes, ?Tail
	    read_to_codes/4,		% +Stream, +Count, -Read, -Codes
	    read_to_codes/5		% +Stream, +Count, -Read, -Codes, ?Tail
	  ]).
