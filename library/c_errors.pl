:- module(c_errors,
	  [ type_error/2
	  ]).

type_error(Valid_Type,Culprit):- throw(error(type_error(Valid_Type,Culprit),_).
