:- module(atoms,
	  [ atomic_concat/3,		% +,+,-
	    atomic_list_concat/2,	% +list(atomic), -atom
	    atomic_list_concat/3,	% +list(atomic), +atom, -atom
	    atom_split/3,		% +atom, +list(chars), -list(atom)
	    atom_number/2,		% Atom <-> Number
	    number_digits/2,		% Number <-> list(integer|punct)
	    				% == numberchars + make digits
	    upcase_atom/2,		% +atom, -atom
	    downcase_atom/2,		% +atom, -atom
	    atom_property/2		% +atom, ?property
	    				% quotes, etc.
	  ]).

:- include(library('dialect/commons')).

% TODO: This module does not contain any implementation yet.

%! @title Atom utilities
%
%  @module
%
%  This module contains predicates for handling atoms and sometimes atomics.
%
%  @author

% :- pred atom_property(?atom, ?atom_propery) + nondet # "retrieve atom properties"


% get atom properties
%
%	atom_property(Atom, Property) succeeds if current_atom(Atom) succeeds
%	and if Property unifies with one of the properties of the atom.
%	This predicate is re-executable on backtracking.
%
%	Atom properties:
%
%	 * length(Length): Length is the length of the name of the atom.
%	 * hash(Hash): Hash is the internal key of the atom.
%	 * needs_quotes: if the atom must be quoted to be read later.
%	 * needs_scan: if the atom must be scanned when output to be read later
%	   (e.g. contains special characters that must be output with a \
%	    escape sequence).
%
%	Remark hash(Hash) can also be obtain using term_hash/2.


	
