:- module(c_gensym,
	  [ gensym/2,
	    reset_gensym/0,
	    reset_gensym/1,
	    
	    gennum/2,
	    reset_gennum/0,
	    reset_gennum/1,
	    
	    newsym/2			% G-Prolog ?3
	  ]).

% gensym and gennum are global (thread and module-wise).
