
%1 statistics(?Key,?Value)
%% 
%% global_stack [size used,free 
%% This refers to the global stack, where compound terms are stored. The
%% values are gathered before the list holding the answers is allocated.
%% 
%% local_stack [size used,free]
%% This refers to the local stack, where recursive predicate environments are stored.
%% 
%% trail [size used,free]
%% This refers to the trail stack, where conditional variable bindings are recorded.
%% 
%% choice
%% [size used,free] 
%% This refers to the choicepoint stack, where partial states are stored for backtracking purposes.core 
%% 
%% memory [size used,0] These refer to the amount of memory actually allocated by the process.
%% 
%% heap
%% 
%% program
%% [size used,0]
%% These refer to the amount of memory allocated for compiled and interpreted clauses, symbol tables, and the like.
%% 
%% runtime [since start of Prolog,since previous statistics] 
%% These refer to CPU time used while executing, excluding time spent
%% garbage collecting, stack shifting, or in system calls. In Muse, these
%% numbers refer to the worker that happens to be executing the call to
%% statistics/2, and so normally are not meaningful.
%% 
%% user_time [since start of Prolog,since previous statistics] 
%% These refer to CPU time used while executing, excluding time spent
%% garbage collecting, stack shifting, or in system calls. In Muse, these
%% numbers refer to the worker that happens to be executing the call to
%% statistics/2, and so normally are not meaningful.
%% 
%% system_time: [since start of Prolog,since previous statistics] 
%% time in system calls
%% 
%% walltime
%% [since start of Prolog,since previous statistics] These refer to absolute time elapsed.
%% 
%% garbage_collection
%% [no. of GCs,bytes freed,time spent]
%% 
%% stack_shifts
%% [no. of local shifts,no. of trail shifts,time spent]
%% 
%% atoms
%% [no. of atoms,bytes used,bytes free]
%% 
%% atom_garbage_collection
%% [no. of AGCs,bytes freed,time spent]
