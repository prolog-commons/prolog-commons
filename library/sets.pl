:- module(c_sets,
	  [ intersection/3,
	    union/3,
	    difference/3,		% +All, +Not, -Rest
	    subset/2
	  ]).
:- include(library('dialect/commons')).

