:- module(c_arith,
	  [ between/3,			% +int, +int, ?int
	    succ/2			% int <-> int
	  ],
	  [commons]).

:- if(\+ feature(implementation_defined(succ/2))).

% XSB succ/2 -- please replace if there is a better one to use.
% TLS: not putting in errors (they are caught by is/2).
succ(First,Second):-
    (nonvar(First) ->
         Second is First + 1
      ; (nonvar(Second) ->
	Second > 0,
	  First is Second - 1
	 ; Second is First + 1)).

:- endif.
