:- module(c_coroutining,
	  [ freeze/2,
	    frozen/2,
	    when/2,
	    dif/2,
	    copy_term/3
	  ]).
