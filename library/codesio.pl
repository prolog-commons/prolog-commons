:- module(c_codesio,
          [ format_to_codes/3,          % +Format, +Args, -Codes
            format_to_codes/4,          % +Format, +Args, -Codes, ?Tail
            write_to_codes/2,           % +Term, -Codes
            write_to_codes/3,           % +Term, -Codes, ?Tail
            atom_to_codes/2,            % +Atom, -Codes
            atom_to_codes/3,            % +Atom, -Codes, ?Tail
            number_to_codes/2,          % +Number, -Codes
            number_to_codes/3,          % +Number, -Codes, ?Tail
                                        % read predicates
            read_from_codes/2,          % +Codes, -Term
            open_codes_stream/2,        % +Codes, -Stream
            with_output_to_codes/2,     % :Goal, -Codes
            with_output_to_codes/3,     % :Goal, -Codes, ?Tail
            with_output_to_codes/4      % :Goal, -Stream, -Codes, ?Tail
          ]).

:- include(library('dialect/commons')).
