#!/bin/bash
#
# Simple script to generate documentation for the libraries and the web site
#
# Manuel Hermenegildo <herme@fi.upm.es>
# Jose F. Morales <jfran@clip.dia.fi.upm.es>
#
# ---------------------------------------------------------------------------
# Obtain the directory this script lives in.
old_dir=`pwd`; cd `dirname $0`; self=`pwd`; cd ${old_dir}; old_dir=

if [ x"${COMMONS_LPDOC}" == x"" ]; then
    lpdoc=${self}/systems/ciao/build/bin/lpdoc
else
    lpdoc=${COMMONS_LPDOC}
fi
doc=${self}/doc
web=${self}/web
mainfname=PrologCommons.html
cssfile=${web}/lpdoc_commons.css
websitehost="software.imdea.org"
websitedir="/srv/www/prolog-commons/remote"
websiteuser="herme"
websitegroup="prolog-commons-local"
# webtarget=${self}/public_html

update_lpdoc() {
    export COMMONS_CIAO_VERSION=SVN
    ${self}/common-prolog.sh fetch ciao 
    pushd ${self}/systems/ciao/ciao > /dev/null 
    ./ciaosetup clean_config
    ./ciaosetup configure --instype=local --registration_type=user
    ./ciaosetup build engine
    ./ciaosetup build ciaoc
    ./ciaosetup build libraries
    ./ciaosetup build lpdoc
    popd > /dev/null
}

lpdoc() {
    if [ -x ${lpdoc} ]; then
	pushd ${doc} > /dev/null 
	${lpdoc} ${opts} -d "commonsroot=${self}/" -d "lpdoclib=${self}/systems/ciao/lpdoc/lib/" \
            -d "ciaolibroot=${self}/systems/ciao/ciao/" $1
	if [ -d ${mainfname} ]; then
	    cp -f ${cssfile} ${mainfname}/lpdoc.css
	fi
	popd > /dev/null
    else
	echo "ERROR: lpdoc (${lpdoc}) could not be run - perhaps Ciao has not been installed?"
    fi
}

# webtarget() {
#     pushd ${webtarget} > /dev/null 
#     ${lpdoc} ${opts} -d "commonsroot=${self}" -f ${self}/web/SETTINGS.pl $1
#     popd > /dev/null
# }

help() {
    cat <<EOF

Usage: `basename ${0}` [-v] [ pdf | html | clean | realclean | publish | update_lpdoc ]
EOF
}

publish() {
    local target
    local sshcommand
    local sshe
    case "$1" in 
	remote)
	    target=${websitehost}:
	    sshcommand="ssh ${websitehost}"
	    essh="-e ssh"
	    ;;
	here)
	    target=""
	    sshcommand=""
	    essh=""
	    ;;
    esac
    echo ":: Copying files using: rsync -av ${essh} ... ${target}${websitedir}."
    pushd ${web} > /dev/null 
    rsync -av ${essh} index.html commons-logo.png ${target}${websitedir}
    popd > /dev/null
    pushd ${doc} > /dev/null 
    rsync -av ${sshe} ${mainfname} ${target}${websitedir}
    rsync -av ${sshe} ${cssfile} ${target}${websitedir}/${mainfname}/lpdoc.css
    popd > /dev/null
    echo ":: Setting permissions using: ${sshcommand} chmod/chown ${websiteuser}.${websitegroup} ${websitedir}"
    # Minimizing number of rapid consecutive sshs (to avoid blocking)
    ${sshcommand} "chmod -R ug+rwX ${websitedir}; chmod -R o+rX ${websitedir}; chown -R ${websiteuser}.${websitegroup} ${websitedir}"
}

cleanweb() {
    local sshcommand
    case "$1" in 
	remote) sshcommand="ssh ${websitehost}" ;;
	here)   sshcommand="" ;;
    esac
    echo ":: Cleaning web using: ${sshcommand} rm -rf ${websitedir}/*"
    ${sshcommand} "rm -rf ${websitedir}/*"
}

opts=""
case $1 in
    -v)
	shift; opts="${opts} -v" ;;
    *)
esac

case $1 in
    pdf)           shift; lpdoc pdf ;;
    html)          shift; lpdoc html ;;
    clean)         shift; lpdoc clean ;;
    realclean)     shift; lpdoc realclean ;;
    publish)       shift; cleanweb remote; publish remote;;
    publish_local) shift; cleanweb here; publish here;;
## These are for possible future auto generation of web site by lpdoc
#    webhtml)       shift; webtarget html ;;
#    webclean)      shift; webtarget clean ;;
#    webrealclean)  shift; webtarget realclean ;;
    update_lpdoc)  update_lpdoc;;
    *)             help ;;
esac

